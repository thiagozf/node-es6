# Node ES6

Configurações básicas para utilizar ES6 em um novo projeto.

----------

#### Instalar dependências

```
npm install
```


#### Executar projeto

```
npm start
```


#### Rodar testes

```
npm test
```


#### Distribuir app

```
npm build
```
